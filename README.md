#Chegg home assignment - Q-Fetcher
---

This app was written using Spring framework as the Java backend server and a react app at the front end.
### Main class /src/main/java/com/chegg/fetcher/Application.class 

The app peruse is to fetch academic questions, which are provided in several data formats.

results are processed and returned in an aggregated response.

Data source file location
```bash
    data/questions_urls.json
```

---

## Installation

### Running Locally

In order to run locally first clone the project from Bitbucket

```bash
git clone https://eran_reuveni@bitbucket.org/eran_reuveni/home-assignment.git && cd home-assignment
```

Then run gradle task

```bash
./gradlew bootRun
```
---

## Note
The Docker container is not set with 'GOOGLE_APPLICATION_CREDENTIALS' env variable by intention.
If you run the app via a docker container, requests for Google API will fail.
If running locally with 'bootRun' the application will use vaild credentials.

### Running locally on a docker image

There are 2 options for running the image locally:

1. Build the image locally
2. Pull the image from docker hub

---
### (1) Build the image locally

First clone the project as described above, then use [jibDockerBuild] to build the image locally,
For more info about jib see [jib Github Page](https://github.com/GoogleContainerTools/jib)

```bash
./gradlew jibDockerBuild
```

Once the build is finished, run the container

```bash
docker run -p 8080:8080 reuvenier/home-assignment:latest
```

---
### (2) Pull the image from Docker hub

The image is exposed at a Dockerhub repo, in order to run the app from an existing image simply do

 ```bash
 docker run -p 8080:8080 reuvenier/home-assignment:latest
 ```

 And the image will be pulled from the hub.
