package com.chegg.fetcher.controllers;

import com.chegg.fetcher.dtos.QuestionFormat;
import com.chegg.fetcher.dtos.Questions;
import com.google.gson.Gson;
import io.restassured.RestAssured;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.UnsupportedEncodingException;
import java.util.Random;

import static io.restassured.RestAssured.given;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class QuestionsControllerTest {


    private static final int THRESHOLD_QUESTIONS_LIST_SIZE = 5;
    private static final Random RAND = new Random();


    @LocalServerPort
    private int port;



    @Before
    public void setup() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = port;
    }


    @Test
    public void getQuestionsShouldGetAllIfNoParams() throws Exception {

         String questionsJson = given()
                .get("/questions")
                .then().assertThat().statusCode(200).and().extract().body().asString();

        Questions questions = toQuestions(questionsJson);
        Assert.assertTrue("Questions list seems to be smaller than the threshold - Not all questions was listed.", questions.getQuestions().size() >= THRESHOLD_QUESTIONS_LIST_SIZE);

    }

    @Test
    public void getQuestionsShouldFilterIfParamFormatExist() throws UnsupportedEncodingException {
        Questions all = getAll();

        QuestionFormat csvFormat = QuestionFormat.CSV;

        String questionsJson = given()
                .get("/questions?filter=" + csvFormat.name().toLowerCase())
                .then().assertThat().statusCode(200).and().extract().body().asString();

        Questions questions = toQuestions(questionsJson);
        Assert.assertTrue("Questions list after filter is greater than all questions ? hmm", all.getQuestionsSize() >= questions.getQuestionsSize());
        questions.getQuestions().forEach(q -> Assert.assertSame("Questions after filter still contains irrelevant formats of type [" + q + "]", csvFormat, q.getFormat()));
    }

    @Test
    public void getQuestionsShouldLimitPageSizeIfParamExist() throws UnsupportedEncodingException {
        Questions all = getAll();

        int pageSize = all.getQuestionsSize() - 1;

        String questionsJson = given()
                .get("/questions?page_size=" + pageSize)
                .then().assertThat().statusCode(200).and().extract().body().asString();

        Questions questions = toQuestions(questionsJson);

        Assert.assertTrue("Next page does not exist, although requested page size was lower than maximum - should have got a next page token",questions.getPagination().getNext() != null && !questions.getPagination().getNext().isEmpty());
        Assert.assertEquals("Page size is not as expected ", pageSize, questions.getQuestionsSize());
    }

    @Test
    public void getQuestionsShouldGetAllIfPageSizeIsGreaterThanTotal() throws UnsupportedEncodingException {
        Questions all = getAll();

        int pageSize = all.getQuestionsSize() + 1;

        String questionsJson = given()
                .get("/questions?page_size=" + pageSize)
                .then().assertThat().statusCode(200).and().extract().body().asString();

        Questions questions = toQuestions(questionsJson);

        Assert.assertTrue("Next page exist, although requested page size was greater than maximum - should not have got a next page token",questions.getPagination().getNext().isEmpty() );
        Assert.assertEquals("Page size is not as expected ", all.getQuestionsSize(), questions.getQuestionsSize());
    }


    @Test
    public void getQuestionsWithVaildNextPageParamShouldReturnNextQuestions() throws UnsupportedEncodingException {
        Questions all = getAll();

        int pageSize = all.getQuestionsSize() - 1;

        String questionsJson = given()
                .get("/questions?page_size=" + pageSize)
                .then().assertThat().statusCode(200).and().extract().body().asString();

        Questions questions = toQuestions(questionsJson);

        Assert.assertTrue("Next page does not exist, although requested page size was lower than maximum - should have got a next page token",questions.getPagination().getNext() != null && !questions.getPagination().getNext().isEmpty());

        String nextPage = questions.getPagination().getNext();

        String nextQuestionsJson = given()
                .get(nextPage)
                .then().assertThat().statusCode(200).and().extract().body().asString();

        Questions nextQuestions = toQuestions(nextQuestionsJson);

        Assert.assertEquals("Total pages size does not equals to all", nextQuestions.getQuestionsSize() + questions.getQuestionsSize(), all.getQuestionsSize());

    }

    @Test
    public void getQuestionsWithInvalidNextPageParamShouldReturnBadRequest() throws UnsupportedEncodingException {
        given()
                .get("/questions/invalidNextPageToken")
                .then().assertThat().statusCode(400);
    }


    public static Questions toQuestions(String results) throws UnsupportedEncodingException {

        return new Gson().fromJson(results, Questions.class);
    }

    private Questions getAll() throws UnsupportedEncodingException {
        String questionsJson = given()
                .get("/questions")
                .then().assertThat().statusCode(200).and().extract().body().asString();

        return toQuestions(questionsJson);

    }


}