package com.chegg.fetcher.services;

import com.chegg.fetcher.dtos.QuestionFormat;
import com.chegg.fetcher.exceptions.FileFormatException;
import org.junit.Assert;
import org.junit.Test;

public class QuestionFormatResolverTest {

    private static final String STUB_FILE_SUFFIX = "/some/folder/some/file.";
    private QuestionFormatResolver resolver = new QuestionFormatResolver();



    @Test
    public void resolveShouldFindTheFileFormatIfExists() throws FileFormatException {

        QuestionFormat[] availableFormats = QuestionFormat.values();

        for (QuestionFormat expectedFormat : availableFormats){
                QuestionFormat actualFormat = resolver.resolve(STUB_FILE_SUFFIX + expectedFormat.name().toLowerCase());
            Assert.assertEquals("Expected format was not returned.", expectedFormat, actualFormat);
        }
    }

    @Test(expected = FileFormatException.class)
    public void resolveShouldThrowIfFileFormatIsNotSupported() throws FileFormatException {
        resolver.resolve(STUB_FILE_SUFFIX + "no-such-format");
    }

    @Test(expected = FileFormatException.class)
    public void resolveShouldThrowIfNoFormatFound() throws FileFormatException {
        resolver.resolve("no-format-at-all");
    }

}