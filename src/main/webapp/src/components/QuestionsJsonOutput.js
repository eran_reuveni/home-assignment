import React from 'react';
import ReactJson from 'react-json-view'


export default class QuestionsJsonOutput extends React.Component {

    constructor(props){
        super(props);
    }

    render(){
        return <ReactJson src={this.props.json} theme="grayscale:inverted" collapsed={false} displayObjectSize='false' displayDataTypes='false'/>
    }
}