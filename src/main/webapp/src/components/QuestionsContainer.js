import React from 'react';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import QuestionsJsonOutput from './QuestionsJsonOutput';
import QuestionsForm from './QuestionsForm';

export default class QuestionsContainer extends React.Component {


    constructor() {
        super();
        this.state = {
            json: {},
            selectedFormats: [],
            pageSize: 0,
            nextPagePath: null,
            nextPageDisabled: true,
            error: null,
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleSubmitResult = this.handleSubmitResult.bind(this);
        this.handleNextPage = this.handleNextPage.bind(this);
    }

    componentDidMount() {
        fetch("/questions")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        json: result,
                        nextPageDisabled: true,
                    });
                },
                (error) => {
                    this.setState({
                        json: error,
                        error
                    });
                }
            );
    }

    handleSubmit(formats, pageSize) {
        fetch("/questions?filter=" + formats.join(",") + "&page_size=" + pageSize)
            .then(res => res.json())
            .then(this.handleSubmitResult,this.handleSubmitError);
    }

    handleSubmitResult(result) {
        let isPaginationNextExists = result.pagination !== undefined && result.pagination.next !== undefined && result.pagination.next !== "";
        let nextPage = isPaginationNextExists ? result.pagination.next : "";
        this.setState({
            json: Object.assign(result),
            nextPageDisabled: !isPaginationNextExists,
            nextPagePath: nextPage,
        });
    }

    handleSubmitError(error) {
        this.setState({
            json: error,
            nextPageDisabled: true,
            error
        });
    }

    handleNextPage() {
        fetch(this.state.nextPagePath)
            .then(res => res.json())
            .then(this.handleSubmitResult,
                this.handleSubmitError
            );
    }


    render() {
        let qpanel = <QuestionsForm
            nextPageDisabled={this.state.nextPageDisabled}
            handleSubmit={this.handleSubmit}
            handleNextPage={this.handleNextPage}
        ></QuestionsForm>;

        return (
            <Grid container spacing={10} justify='center'>
                <Grid style={{ paddingTop: '5%' }} item xs={8} sm={4}>
                    {qpanel}
                </Grid>

                <Grid style={{ paddingTop: '5%' }} item xs={10} sm={6}>

                    <QuestionsJsonOutput json={this.state.json}>
                    </QuestionsJsonOutput>

                </Grid>
            </Grid>
        )
    }
}