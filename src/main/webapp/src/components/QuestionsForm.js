import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField'

import PanelIcon from '@material-ui/icons/Code'

const classes = makeStyles(theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
    chip: {
        marginRight: theme.spacing(1),
    },
    section1: {
        margin: theme.spacing(3, 2),
    },
    section2: {
        margin: theme.spacing(2),
    },
    section3: {
        margin: theme.spacing(3, 1, 1),
    },
    textField: {
    },
}));



export default class QuestionsForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
            pageSize: 0,
            selectedFormats: [],
            error: null,
        };

        this.handlePageSizeChange = this.handlePageSizeChange.bind(this);
        this.handleFormats = this.handleFormats.bind(this);
        this.reduceFormats = this.reduceFormats.bind(this);
    }

    componentDidMount(){
        fetch("/questions/formats")
            .then(res => res.json())
            .then(
                (result) => {
                    let selectedFormats = [];
                    selectedFormats.push({key: 'all', selected:true})
                    result.forEach(format=>{
                        selectedFormats.push({key: format, selected:false})
                    })
                    
                    this.setState({
                        selectedFormats: selectedFormats
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    handlePageSizeChange(event) {
        let curr = event.target.value;
        if(curr < 0){
            curr = 0;
        }

        this.setState(
            {
                pageSize: curr,
             } )
    }

    reduceFormats(){
        let selectedFormats = Object.assign(this.state.selectedFormats);
        let formats = [];

        selectedFormats.forEach(format=>{
            if(format.selected && format.key !== 'all'){
                formats.push(format.key);
            }
        })
        return formats;
    }

    handleFormats(format){
        let selected = Object.assign(this.state.selectedFormats)
        if(format.key == 'all'){
            selected = this.selectAll();
        }
        else{
            let isFormatSelected = false;
            selected.forEach(selection=>{
                if(selection.key === format.key){
                    selection.selected = !selection.selected;
                }

                if(selection.key === 'all' && selection.selected){
                    selection.selected = !selection.selected;
                }

                isFormatSelected = isFormatSelected || selection.selected;
            })
            if(!isFormatSelected){
                selected = this.selectAll();
            }
        }
        this.setState({
            selectedFormats : selected,
        });
    }

    selectAll(){
        let selected = Object.assign(this.state.selectedFormats)

        selected.forEach(selection=>{
            if(selection.key === 'all'){
                selection.selected = true;
            }
            else{
                selection.selected = false;
            }
        })
        return selected;
    }


    render() {

        return (
            <div className={classes.root}>
                <div className={classes.section1}>
                    <Grid container alignItems="center">
                        <Grid item xs>
                            <Typography gutterBottom variant="h4">
                                Q-Panel
                    </Typography>
                        </Grid>
                        <Grid item>
                            <Typography gutterBottom variant="h6">
                                <PanelIcon></PanelIcon>
                            </Typography>
                        </Grid>
                    </Grid>
                    <TextField
                        id="standard-number"
                        label="page size"
                        value={this.state.pageSize}
                        onChange={this.handlePageSizeChange}
                        type="number"
                        className={classes.textField}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        fullWidth={false}
                        margin="normal"
                        variant='standard'
                    />
                </div>

                <div style={{ paddingBottom: '5%' }} className={classes.section2}>
                    <Typography gutterBottom variant="body1">
                        Select format type
                </Typography>
                    <div>
                        {this.state.selectedFormats.map(format => (
                            <Chip style={{margin:'1%'}} className={classes.chip} color={format.selected ? "primary" : "default"} label={format.key} clickable={true} onClick={()=>{this.handleFormats(format)} }  />
                        ))}
                    </div>
                </div>

                <Divider variant="fullWidth" />
                <div style={{ paddingTop: '5%' }} className={classes.section3}>
                    <Button color="primary" onClick={()=>this.props.handleSubmit(this.reduceFormats(), this.state.pageSize)}>Submit</Button>
                    <Button disabled={this.props.nextPageDisabled} onClick={this.props.handleNextPage} color="secondary">Next Page</Button>
                </div>
            </div>
        );
    }
}