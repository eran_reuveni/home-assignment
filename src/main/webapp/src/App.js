import React from 'react';
import Container from '@material-ui/core/Container'
import QuestionsContainer from './components/QuestionsContainer'
import { makeStyles } from '@material-ui/core/styles';
import AppBarFilter from './components/AppBarFilter'

export default function App() {

  return (
    <div className="appContainer">
    <AppBarFilter></AppBarFilter>
    <QuestionsContainer></QuestionsContainer>
    </div>
  
  )
}

