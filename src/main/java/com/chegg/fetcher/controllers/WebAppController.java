package com.chegg.fetcher.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Entry point for the web app
 * Web app can be found at /src/main/webapp
 */
@Controller
public class WebAppController {

    @RequestMapping("/")
    public String index() {
        return "redirect:/index.html";
    }

}
