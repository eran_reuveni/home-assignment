package com.chegg.fetcher.controllers;

import com.chegg.fetcher.dtos.QuestionFormat;
import com.chegg.fetcher.dtos.Questions;
import com.chegg.fetcher.services.QuestionsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


@RestController
public class QuestionsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(QuestionsController.class);

    @Autowired
    private QuestionsService questions;


    @RequestMapping(value = "/questions", method = RequestMethod.GET, produces = {"application/json"})
    public Callable<Questions> questions(@RequestParam(value = "filter", required = false, defaultValue = "") List<String> formatTypes, @RequestParam(value = "page_size", defaultValue = "0") Integer pageSize) {
        return () -> questions.getQuestions(toQuestionFormatList(formatTypes), pageSize);
    }

    @RequestMapping(value = "/questions/{next_page}", method = RequestMethod.GET, produces = {"application/json"})
    public Callable<Questions> questionsByPageToken(@PathVariable(name = "next_page") String nextPageToken) {
        return () -> questions.getQuestions(nextPageToken);
    }

    @RequestMapping(value = "/questions/formats", method = RequestMethod.GET, produces = {"application/json"})
    public List<QuestionFormat> getAllFormats() {
        LOGGER.info("Listing available formats");
        return List.of(QuestionFormat.values());
    }

    private static List<QuestionFormat> toQuestionFormatList(List<String> formats) {
        List<QuestionFormat> types = new ArrayList<>();
        if (formats != null) {
            try {
                formats.forEach(format -> types.add(QuestionFormat.valueOf(format.toUpperCase())));
            } catch (Exception e) {
                LOGGER.error("Failed to parse question format", e);
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No such question format");
            }
        }
        return types;
    }

}
