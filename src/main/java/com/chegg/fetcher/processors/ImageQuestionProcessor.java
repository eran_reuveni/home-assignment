package com.chegg.fetcher.processors;

import com.chegg.fetcher.dtos.Question;
import com.chegg.fetcher.dtos.QuestionFormat;
import com.chegg.fetcher.exceptions.QuestionProcessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Processing images response from Google cloud Vision API
 */
@Component
public class ImageQuestionProcessor implements QuestionProcessor<List<String>> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImageQuestionProcessor.class);


    public ImageQuestionProcessor() {
    }

    @Override
    public List<Question> process(List<String> data) throws QuestionProcessException {
        LOGGER.debug("Got Data from google cloud \n {}", data);

        List<Question> questions = new ArrayList<>();
        try {
            data.forEach(q -> questions.add(buildQuestionFromTxt(q)));
            return questions;

        } catch (Exception e) {
            String err = String.format("Failed to fetch data from google cloud for URL [%s]", data);
            LOGGER.error(err);
            throw new QuestionProcessException(err, e);
        }
    }

    private Question buildQuestionFromTxt(String txt) {
        return Question.create().with($ -> {
            $.format = QuestionFormat.PNG;
            $.value = txt;
        }).build();
    }
}
