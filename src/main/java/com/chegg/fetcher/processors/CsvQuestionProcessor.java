package com.chegg.fetcher.processors;

import com.chegg.fetcher.exceptions.QuestionProcessException;
import com.chegg.fetcher.dtos.Question;
import com.chegg.fetcher.dtos.QuestionFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.chegg.fetcher.utils.FunctionalUtils.exceptionWrapper;

@Component
public class CsvQuestionProcessor implements QuestionProcessor<String> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CsvQuestionProcessor.class);


    public CsvQuestionProcessor() {
    }

    @Override
    public List<Question> process(String data) throws QuestionProcessException {
        ArrayList<String> csvQuestionsLines = new ArrayList<>(Arrays.asList(data.split("\n")));
        csvQuestionsLines.remove(0);

        return csvQuestionsLines.stream().map(exceptionWrapper(this::extractQuestionFromCsvLine)).collect(Collectors.toList());
    }


    private Question extractQuestionFromCsvLine(String csvLine) throws QuestionProcessException {
        String[] line = csvLine.split("\\s*,\\s*");

        if (line.length < 2) {
            String err = String.format("Failed to process csv line %s", csvLine);
            LOGGER.error(err);
            throw new QuestionProcessException(err);
        }

        return Question.create().with($ -> {
            $.format = QuestionFormat.CSV;
            $.value = line[1];
        }).build();
    }


}
