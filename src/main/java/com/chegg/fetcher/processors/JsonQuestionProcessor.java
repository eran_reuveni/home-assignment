package com.chegg.fetcher.processors;

import com.chegg.fetcher.exceptions.QuestionProcessException;
import com.chegg.fetcher.dtos.Question;
import com.chegg.fetcher.dtos.QuestionFormat;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@Component
public class JsonQuestionProcessor implements QuestionProcessor<String> {

    private final Gson jsonParser;

    private final Type listType = new TypeToken<List<Question>>() {
    }.getType();

    @Autowired
    public JsonQuestionProcessor() {
        this.jsonParser = new GsonBuilder().registerTypeAdapter(listType, new QuestionsDeserializer()).create();
    }

    @Override
    public List<Question> process(String data) throws QuestionProcessException {
        try {
            return jsonParser.fromJson(data, listType);
        } catch (Exception e) {
            throw new QuestionProcessException("Failed to parse json data to object", e);
        }
    }

    public static class QuestionsDeserializer implements JsonDeserializer<List<Question>> {

        public List<Question> deserialize(JsonElement je, Type type, JsonDeserializationContext jdc) throws JsonParseException {
            JsonArray data = je.getAsJsonObject().getAsJsonArray("questions");
            ArrayList<Question> myList = new ArrayList<>();

            for (JsonElement e : data) {

                String txt = e.getAsJsonObject().get("text").getAsString();
                Question q = Question.create().with($ -> {
                    $.format = QuestionFormat.JSON;
                    $.value = txt;
                }).build();
                myList.add(q);
            }

            return myList;

        }

    }
}
