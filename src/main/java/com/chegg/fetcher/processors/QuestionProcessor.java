package com.chegg.fetcher.processors;

import com.chegg.fetcher.exceptions.QuestionProcessException;
import com.chegg.fetcher.dtos.Question;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Responsible for parsing the retrieved data from {@link com.chegg.fetcher.providers.QuestionsProvider}
 *
 * @param <T> raw type data
 */
@Component
public interface QuestionProcessor<T> {
    List<Question> process(T data) throws QuestionProcessException;
}
