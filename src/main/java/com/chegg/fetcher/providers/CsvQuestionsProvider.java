package com.chegg.fetcher.providers;

import com.chegg.fetcher.clients.http.WebRequestImpl;
import com.chegg.fetcher.clients.http.interfaces.MethodType;
import com.chegg.fetcher.clients.http.interfaces.WebRequest;
import com.chegg.fetcher.clients.http.interfaces.WebRequester;
import com.chegg.fetcher.exceptions.QuestionProcessException;
import com.chegg.fetcher.dtos.Question;
import com.chegg.fetcher.dtos.QuestionFormat;
import com.chegg.fetcher.processors.CsvQuestionProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class CsvQuestionsProvider extends QuestionsProviderImpl {

    private static final Logger LOGGER = LoggerFactory.getLogger(CsvQuestionsProvider.class);

    @Autowired
    private WebRequester http;

    @Autowired
    private CsvQuestionProcessor processor;

    @Autowired
    public CsvQuestionsProvider(WebRequester requester, CsvQuestionProcessor processor) {
        super(QuestionFormat.CSV);
        this.processor = processor;
        this.http = requester;
    }

    @Override
    public List<Question> getQuestions() {
        List<Question> questions = new ArrayList<>();

        for (String url : urls) {

            WebRequest request = WebRequestImpl.create().with($ -> {
                $.method = MethodType.GET;
                $.url = url;
            }).build();

            try {
                String csvQuestions = http.dispatch(request).getResponseBody();
                questions.addAll(processor.process(csvQuestions));

            } catch (QuestionProcessException parseException) {
                LOGGER.error("Failed process data from URL [{}]", url, parseException);
            } catch (NoSuchMethodException | IOException e) {
                LOGGER.error("Failed to send dispatch to URL [{}]", url, e);
            }
        }
        return questions;

    }
}
