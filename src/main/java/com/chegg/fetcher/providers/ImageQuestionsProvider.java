package com.chegg.fetcher.providers;

import com.chegg.fetcher.dtos.Question;
import com.chegg.fetcher.dtos.QuestionFormat;
import com.chegg.fetcher.processors.ImageQuestionProcessor;

import com.google.cloud.vision.v1.AnnotateImageRequest;
import com.google.cloud.vision.v1.AnnotateImageResponse;
import com.google.cloud.vision.v1.BatchAnnotateImagesResponse;
import com.google.cloud.vision.v1.Feature;
import com.google.cloud.vision.v1.Image;
import com.google.cloud.vision.v1.ImageAnnotatorClient;
import com.google.cloud.vision.v1.ImageSource;
import org.apache.http.client.ClientProtocolException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ImageQuestionsProvider extends QuestionsProviderImpl {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImageQuestionsProvider.class);
    private final ImageQuestionProcessor processor;

    @Autowired
    public ImageQuestionsProvider(ImageQuestionProcessor processor) {
        super(QuestionFormat.PNG);
        this.processor = processor;
    }

    @Override
    public List<Question> getQuestions() {
        try {
            List<String> questions = detectTextFromImageUrls();
            return processor.process(questions);
        } catch (Exception e) {
            LOGGER.error("Failed process data from GCS", e);
            return new ArrayList<>();
        }
    }

    private List<String> detectTextFromImageUrls() throws Exception {

        List<String> questions = new ArrayList<>();
        List<AnnotateImageRequest> requests = new ArrayList<>();
        Feature feat = Feature.newBuilder().setType(Feature.Type.TEXT_DETECTION).build();

        urls.forEach(url -> {
            ImageSource imgSource = ImageSource.newBuilder().setImageUri(url).build();
            Image img = Image.newBuilder().setSource(imgSource).build();
            AnnotateImageRequest request = AnnotateImageRequest.newBuilder().addFeatures(feat).setImage(img).build();
            requests.add(request);
        });

        try (ImageAnnotatorClient client = ImageAnnotatorClient.create()) {
            BatchAnnotateImagesResponse response = client.batchAnnotateImages(requests);
            List<AnnotateImageResponse> responses = response.getResponsesList();

            for (AnnotateImageResponse res : responses) {
                if (res.hasError()) {
                    LOGGER.error("Error while processing GCS response: {}\n", res.getError().getMessage());
                    throw new ClientProtocolException("Failed to retrieve questions from GCS");
                }

                if (res.getTextAnnotationsCount() == 0) {
                    LOGGER.error("Failed to get questions text from image, text annotations are empty, no text exists on image");
                } else {
                    questions.add(res.getTextAnnotations(0).getDescription());
                }
            }
        }
        return questions;
    }
}
