package com.chegg.fetcher.providers;

import com.chegg.fetcher.dtos.Question;
import com.chegg.fetcher.dtos.QuestionFormat;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Responsible for retrieving questions from the accepted source.
 */
@Component
public interface QuestionsProvider {

    /**
     * @return all of the provider questions by available source
     */
    List<Question> getQuestions();


    /**
     * @return provider declared format
     */
    QuestionFormat getProviderFormat();

    /**
     * Set the source for the provider data
     *
     * @param source
     */
    void setSource(String source);

}
