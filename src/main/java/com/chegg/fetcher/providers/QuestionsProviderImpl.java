package com.chegg.fetcher.providers;

import com.chegg.fetcher.dtos.QuestionFormat;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public abstract class QuestionsProviderImpl implements QuestionsProvider {

    private final QuestionFormat format;

    protected List<String> urls;

    public QuestionsProviderImpl(QuestionFormat format) {
        this.format = format;
        this.urls = new ArrayList<>();
    }

    @Override
    public QuestionFormat getProviderFormat() {
        return format;
    }

    @Override
    public void setSource(String url) {
        this.urls.add(url);
    }
}
