package com.chegg.fetcher.providers;

import com.chegg.fetcher.clients.http.WebRequestImpl;
import com.chegg.fetcher.clients.http.interfaces.MethodType;
import com.chegg.fetcher.clients.http.interfaces.WebRequest;
import com.chegg.fetcher.clients.http.interfaces.WebRequester;
import com.chegg.fetcher.dtos.Question;
import com.chegg.fetcher.dtos.QuestionFormat;
import com.chegg.fetcher.processors.JsonQuestionProcessor;
import com.google.gson.JsonSyntaxException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class JsonQuestionsProvider extends QuestionsProviderImpl {

    private static final Logger LOGGER = LoggerFactory.getLogger(JsonQuestionsProvider.class);

    private final WebRequester http;
    private final JsonQuestionProcessor processor;

    @Autowired
    public JsonQuestionsProvider(WebRequester requester, JsonQuestionProcessor processor) {
        super(QuestionFormat.JSON);
        this.http = requester;
        this.processor = processor;
    }

    @Override
    public List<Question> getQuestions() {

        List<Question> questions = new ArrayList<>();

        for (String url : urls) {

            WebRequest request = WebRequestImpl.create().with($ -> {
                $.method = MethodType.GET;
                $.url = url;
            }).build();

            try {
                String data = http.dispatch(request).getResponseBody();
                questions.addAll(processor.process(data));

            } catch (NoSuchMethodException | IOException e) {
                LOGGER.error("Failed to send dispatch to URL [{}]", url, e);
            } catch (JsonSyntaxException e) {
                LOGGER.error("Failed to parse JSON response!", e);
            }
        }
        //TODO - Should this throw in case some of the data failed to be fetched? or just return an empty list?
        return questions;
    }
}
