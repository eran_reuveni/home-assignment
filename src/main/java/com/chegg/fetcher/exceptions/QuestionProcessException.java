package com.chegg.fetcher.exceptions;

import java.io.IOException;
import java.text.ParseException;

public class QuestionProcessException extends IOException {

    public QuestionProcessException(String message, Throwable cause) {
        super(message, cause);
    }

    public QuestionProcessException(String message) {
        super(message);
    }
}
