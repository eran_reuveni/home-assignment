package com.chegg.fetcher.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * Thrown in case questions next page token is invalid
 */
public class NoSuchTokenException extends ResponseStatusException {

    public NoSuchTokenException(String s) {
        super(HttpStatus.BAD_REQUEST, s);
    }
}
