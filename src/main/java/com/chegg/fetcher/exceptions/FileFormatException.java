package com.chegg.fetcher.exceptions;

import java.io.IOException;

public class FileFormatException extends IOException {

    public FileFormatException(String message, Throwable cause) {
        super(message, cause);
    }

    public FileFormatException(String message) {
        super(message);
    }
}
