package com.chegg.fetcher.clients.http.interfaces;

import org.springframework.stereotype.Component;

/**
 * Abstraction for service HTTP request
 */
@Component
public interface WebRequest {

    String url();

    MethodType method();

    String authentication();

    String body();


}
