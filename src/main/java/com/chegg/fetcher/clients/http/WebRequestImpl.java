package com.chegg.fetcher.clients.http;

import com.chegg.fetcher.clients.http.interfaces.MethodType;
import com.chegg.fetcher.clients.http.interfaces.WebRequest;
import org.springframework.stereotype.Component;

import java.util.function.Consumer;

@Component
public class WebRequestImpl implements WebRequest {

    private String url;
    private MethodType method;
    private String body;
    private String auth;

    private WebRequestImpl(String url, MethodType method, String body, String auth) {
        this.url = url;
        this.method = method;
        this.auth = auth;
    }

    public WebRequestImpl() {
    }

    public static Builder create() {
        return new Builder();
    }

    @Override
    public String url() {
        return url;
    }

    @Override
    public MethodType method() {
        return method;
    }

    @Override
    public String authentication() {
        return auth;
    }

    @Override
    public String body() {
        return null;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setMethod(MethodType method) {
        this.method = method;
    }

    public static class Builder {

        public String url;
        public MethodType method;
        public String auth;
        public String body;

        public Builder with(Consumer<Builder> builderFunction) {
            builderFunction.accept(this);
            return this;
        }

        public WebRequestImpl build() {
            return new WebRequestImpl(url, method, body, auth);
        }
    }
}
