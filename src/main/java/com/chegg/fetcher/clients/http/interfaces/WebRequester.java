package com.chegg.fetcher.clients.http.interfaces;

import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Abstraction for HTTP client
 */
@Component
public interface WebRequester {

    WebResponse dispatch(WebRequest request) throws NoSuchMethodException, IOException;
}
