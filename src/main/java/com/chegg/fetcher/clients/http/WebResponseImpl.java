package com.chegg.fetcher.clients.http;

import com.chegg.fetcher.clients.http.interfaces.WebResponse;
import org.springframework.stereotype.Component;

import java.util.function.Consumer;

@Component
public class WebResponseImpl implements WebResponse {

    private final int status;

    private final String responseBody;
    private final String responseStatusMessage;


    private WebResponseImpl(int status, String responseBody, String responseStatusMessage) {
        this.status = status;
        this.responseBody = responseBody;
        this.responseStatusMessage = responseStatusMessage;

    }

    public WebResponseImpl() {
        this.responseBody = "";
        this.responseStatusMessage = "";
        this.status = -1;
    }

    public static Builder create(){
        return new Builder();
    }

    @Override
    public String getResponseBody() {
        return responseBody;
    }

    @Override
    public int getResponseStatus() {
        return status;
    }

    @Override
    public String getResponseStatusMessage() {
        return responseStatusMessage;
    }

    public static class Builder {

        public int status;
        public String responseBody;
        public String responseStatusMessage;


        public Builder with(Consumer<Builder> builderFunction) {
            builderFunction.accept(this);
            return this;
        }

        public WebResponseImpl build() {
           return new WebResponseImpl(status, responseBody, responseStatusMessage);
        }
    }
}
