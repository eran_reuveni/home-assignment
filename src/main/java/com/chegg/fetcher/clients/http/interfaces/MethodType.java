package com.chegg.fetcher.clients.http.interfaces;

public enum MethodType {

    GET,
    POST
}
