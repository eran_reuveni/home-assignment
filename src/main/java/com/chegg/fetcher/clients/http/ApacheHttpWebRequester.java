package com.chegg.fetcher.clients.http;

import com.chegg.fetcher.clients.http.interfaces.MethodType;
import com.chegg.fetcher.clients.http.interfaces.WebRequest;
import com.chegg.fetcher.clients.http.interfaces.WebRequester;
import com.chegg.fetcher.clients.http.interfaces.WebResponse;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.io.IOException;

/**
 * Concrete implementation of service HTTP client using {@link org.apache.http.client.HttpClient}
 */
@Component
public class ApacheHttpWebRequester implements WebRequester {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApacheHttpWebRequester.class);

    private CloseableHttpClient httpclient;


    public ApacheHttpWebRequester() {
        httpclient = HttpClients.createDefault();
    }

    @Override
    public WebResponse dispatch(WebRequest request) throws NoSuchMethodException, IOException {
        return httpclient.execute(toApacheRequest(request), new ApacheResponseHandler());
    }

    private HttpRequestBase toApacheRequest(WebRequest request) throws NoSuchMethodException {
        MethodType method = request.method();
        HttpRequestBase requestBase;
        switch (method) {
            case GET:
                requestBase = new HttpGet(request.url());
                break;
            case POST:
                requestBase = new HttpPost(request.url());
                break;
            default:
                String err = String.format("[%s] Method is not supported", request.method().toString());
                LOGGER.error(err);
                throw new NoSuchMethodException();
        }

        String auth = request.authentication();

        if (auth != null && !auth.isEmpty()) {
            requestBase.setHeader("Authorization", auth);
        }
        return requestBase;
    }


    @PreDestroy
    public void closeClient() throws Exception {
        if (httpclient != null) {
            httpclient.close();
        }
    }


    public static class ApacheResponseHandler implements ResponseHandler<WebResponse> {

        @Override
        public WebResponse handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
            int status = response.getStatusLine().getStatusCode();
            if (status != 200) {
                LOGGER.error(response.toString());
                throw new ClientProtocolException("Unexpected response status");
            }

            HttpEntity entity = response.getEntity();
            String responseBody = entity != null ? EntityUtils.toString(entity) : null;

            return WebResponseImpl.create().with($ -> {
                $.status = status;
                $.responseStatusMessage = response.getStatusLine().toString();
                $.responseBody = responseBody;
            }).build();

        }
    }
}
