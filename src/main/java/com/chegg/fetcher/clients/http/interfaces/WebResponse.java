package com.chegg.fetcher.clients.http.interfaces;

import org.springframework.stereotype.Component;

/**
 * Abstraction for service HTTP response
 */
@Component
public interface WebResponse {

    String getResponseBody();

    int getResponseStatus();

    String getResponseStatusMessage();
}
