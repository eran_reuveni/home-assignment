package com.chegg.fetcher.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum QuestionFormat {

    @JsonProperty("png")
    @SerializedName("png")
    PNG,

    @JsonProperty("json")
    @SerializedName("json")
    JSON,

    @JsonProperty("csv")
    @SerializedName("csv")
    CSV
}
