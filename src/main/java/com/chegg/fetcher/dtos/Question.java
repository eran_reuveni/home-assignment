package com.chegg.fetcher.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.util.function.Consumer;

/**
 * Question data transfer object
 */
public class Question {

    private String value;

    @JsonProperty("source")
    @SerializedName("source")
    private QuestionFormat format;

    public static Builder create() {
        return new Builder();
    }


    public Question(String value, QuestionFormat format) {
        this.value = value;
        this.format = format;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public QuestionFormat getFormat() {
        return format;
    }

    public void setFormat(QuestionFormat format) {
        this.format = format;
    }

    public static class Builder {
        public String value;
        public QuestionFormat format;

        public Builder with(Consumer<Builder> builderFunction) {
            builderFunction.accept(this);
            return this;
        }

        public Question build() {
            return new Question(value, format);
        }
    }
}
