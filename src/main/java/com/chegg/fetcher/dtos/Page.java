package com.chegg.fetcher.dtos;

/**
 * Data transfer object for paging on questions
 */
public class Page {

    private String next;

    public Page(String next) {
        this.next = next;
    }

    public static Page CreateEmptyPage() {
        return new Page("");
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }
}
