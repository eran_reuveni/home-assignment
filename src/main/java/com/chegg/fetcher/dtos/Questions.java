package com.chegg.fetcher.dtos;

import java.util.List;

/**
 * Questions data transfer object
 * Includes paging object
 */
public class Questions {

    private List<Question> questions;

    private Page pagination;

    public Questions(List<Question> questions, Page pagination) {
        this.questions = questions;
        this.pagination = pagination;
    }

    public Questions(List<Question> questions) {
        this.questions = questions;
        this.pagination = new Page("");
    }

    public Questions() {
    }

    public List<Question> getQuestions() {
        return questions;
    }


    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }


    public Page getPagination() {
        return pagination;
    }

    public void setPagination(Page pagination) {
        this.pagination = pagination;
    }

    public int getQuestionsSize(){
        return this.questions.size();
    }
}
