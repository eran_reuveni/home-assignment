package com.chegg.fetcher.services;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

@Component
public class QuestionsDataSourceLoader {

    private static final String URLS_JSON = "/data/questions_urls.json";


    private List<String> sources;

    public QuestionsDataSourceLoader() throws FileNotFoundException {

        JsonReader reader = new JsonReader(new FileReader(getClass().getResource(URLS_JSON).getPath()));
        this.sources =  new Gson().fromJson(reader, new TypeToken<List<String>>() {
        }.getType());
    }

    public List<String> getSources() {
        return sources;
    }

    public void setSources(List<String> sources) {
        this.sources = sources;
    }
}


