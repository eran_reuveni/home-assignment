package com.chegg.fetcher.services;

import com.chegg.fetcher.dtos.Page;
import com.chegg.fetcher.dtos.Question;
import com.chegg.fetcher.dtos.QuestionFormat;
import com.chegg.fetcher.dtos.Questions;
import com.chegg.fetcher.exceptions.FileFormatException;
import com.chegg.fetcher.exceptions.NoSuchTokenException;
import com.chegg.fetcher.providers.QuestionsProvider;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.*;

/**
 * Questions service
 * Loads all of the questions sources from {@link QuestionsDataSourceLoader}
 * Using {@link QuestionFormatResolver} detect format and apply it to the appropriate provider,
 * Using provider {@link QuestionsProvider#getProviderFormat()}
 *
 * In each call to get questions, all providers will send an async request to fetch data from their sources.
 * The service will handle cases of paging and filtering.
 * When a format filter is specified (one or more), the service will filter all other types of format and keep the selected
 * In case of a paged request, the service will cache the request using {@link PageInfo#toBase64Json()} as a key
 */
@Service
public class QuestionsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(QuestionsService.class);

    private final Long appTimeout;

    private final TimeUnit unit = TimeUnit.MILLISECONDS;

    private final QuestionFormatResolver formatResolver;

    private final QuestionsDataSourceLoader dataSourceLoader;

    private final ExecutorService threads;

    private final HashMap<QuestionFormat, QuestionsProvider> questionsProviders;

    private final Cache<String, List<Question>> pageCache;

    @Autowired
    public QuestionsService(QuestionFormatResolver resolver, QuestionsDataSourceLoader dataSourceLoader, List<QuestionsProvider> providers, Environment environment) throws IOException {
        Long maxCacheSize =environment.getProperty("app.max.cache.size", Long.class, 500L);
        this.appTimeout =  environment.getProperty("spring.mvc.async.request-timeout", Long.class, 30000L);
        this.questionsProviders = new HashMap<>();
        this.dataSourceLoader = dataSourceLoader;
        this.pageCache = CacheBuilder.newBuilder().maximumSize(maxCacheSize).build();
        this.threads = Executors.newCachedThreadPool();
        this.formatResolver = resolver;
        registerQuestionsProviders(providers);
        setProvidersSources();
    }

    public Questions getQuestions(final List<QuestionFormat> formats, final Integer pageSize) {
        List<Question> questionsList = getAllQuestions(formats);
        int normalizedPageSize = pageSize > questionsList.size() || pageSize < 0 ? questionsList.size() : pageSize;
        if (normalizedPageSize == 0 || normalizedPageSize == questionsList.size()) {
            return new Questions(questionsList, Page.CreateEmptyPage());
        }

        PageInfo pageInfo = new PageInfo(0, pageSize, questionsList.size(), formats);
        String nextPageToken = pageInfo.toBase64Json();
        String nexPageUrl = pageInfo.getPageSize() == pageInfo.getResultSize() ? "" : "/questions/" + nextPageToken;

        if (!nexPageUrl.isEmpty()) {
            pageCache.put(nextPageToken, questionsList);
        }

        return new Questions(questionsList.subList(pageInfo.getCursor(), pageInfo.getPageSize()), new Page(nexPageUrl));
    }


    public Questions getQuestions(final String nextPageToken) {
        PageInfo pageInfo;
        try {
            pageInfo = PageInfo.fromBase64Json(nextPageToken);
        } catch (Exception e) {
            LOGGER.error("Invalid next page token was provided {}", nextPageToken, e);
            throw new NoSuchTokenException("Invalid next page.");
        }

        List<Question> questionsList = pageCache.getIfPresent(nextPageToken);

        if (questionsList == null) {
            LOGGER.info("Token was not found in cache. [{}]", nextPageToken);
            throw new NoSuchTokenException("next page was not found.");
        }
        pageCache.invalidate(nextPageToken);

        pageInfo.setCursor(pageInfo.getCursor() + pageInfo.getPageSize());
        String newNextPageToken = pageInfo.toBase64Json();
        String nexPageUrl = pageInfo.getCursor() == pageInfo.getResultSize() ? "" : "/questions/" + newNextPageToken;
        int offset = pageInfo.getCursor() + pageInfo.getPageSize() > pageInfo.getResultSize() ? pageInfo.getResultSize() : pageInfo.getCursor() + pageInfo.getPageSize();

        if (!newNextPageToken.isEmpty()) {
            pageCache.put(newNextPageToken, questionsList);
        }

        return new Questions(questionsList.subList(pageInfo.getCursor(), offset), new Page(nexPageUrl));
    }

    private List<Question> getAllQuestions(List<QuestionFormat> formats) {
        List<Future<List<Question>>> futureQuestions = new ArrayList<>();
        List<Question> questions = new ArrayList<>();

        this.questionsProviders.entrySet().stream().filter(entry -> formats.isEmpty() || formats.contains(entry.getKey()))
                .forEach(entry -> futureQuestions.add(threads.submit(() -> entry.getValue().getQuestions())));

        futureQuestions.forEach(future -> {
            try {
                questions.addAll(future.get(calculateProviderTimeout(), unit));

            } catch (InterruptedException | ExecutionException e) {
                LOGGER.error("Failed to execute request to provider.", e);
            } catch (TimeoutException e) {
                LOGGER.error("Got timeout while trying to get response from provider.", e);
            } catch (Exception e) {
                LOGGER.error("Got Exception while retrieve to get provider questions", e);
            }
        });

        return questions;
    }

    private void registerQuestionsProvider(QuestionsProvider provider) {
        if (this.questionsProviders.containsKey(provider.getProviderFormat())) {
            LOGGER.info("Provider of format [{}] is already exist, skipping provider of type {}", provider.getProviderFormat(), provider.getClass());
        } else {
            this.questionsProviders.put(provider.getProviderFormat(), provider);
        }
    }

    private void registerQuestionsProviders(List<QuestionsProvider> providers) {
        providers.forEach(this::registerQuestionsProvider);
    }

    private Long calculateProviderTimeout() {
        int providersCount = this.questionsProviders.values().size();
        return providersCount > 0 ? appTimeout / providersCount : appTimeout;
    }

    private void setProvidersSources() {
        dataSourceLoader.getSources().forEach(url -> {
            try {
                QuestionFormat format = formatResolver.resolve(url);
                QuestionsProvider provider = this.questionsProviders.get(format);

                if (provider != null) {
                    provider.setSource(url);
                } else {
                    LOGGER.warn("No provider was registered for format type [{}]", format);
                }
            } catch (FileFormatException e) {
                LOGGER.error("Failed to find relevant format for URL [{}]", url, e);
            }
        });
    }


    public static class PageInfo {
        private final int pageSize;
        private int cursor;
        private final int resultSize;
        private final List<QuestionFormat> formats;
        private UUID uid;

        static PageInfo fromBase64Json(String base64Json) {
            String json = new String(Base64.getDecoder().decode(base64Json), StandardCharsets.UTF_8);
            return new Gson().fromJson(json, PageInfo.class);
        }

        String toBase64Json() {
            this.uid = UUID.randomUUID();
            String json = new Gson().toJson(this);
            return Base64.getEncoder().withoutPadding().encodeToString(json.getBytes(StandardCharsets.UTF_8));
        }

        PageInfo(int cursor, int pageSize, int resultSize, List<QuestionFormat> formats) {
            this.pageSize = pageSize;
            this.resultSize = resultSize;
            this.cursor = cursor;
            this.formats = formats;
        }

        public boolean isEmpty() {
            return pageSize == 0 && cursor == 0 && resultSize == 0 && formats == null && uid == null;
        }

        public int getPageSize() {
            return pageSize;
        }


        public int getCursor() {
            return cursor;
        }

        public void setCursor(int cursor) {
            cursor = cursor > resultSize ? resultSize : cursor;
            this.cursor = cursor;
        }

        public int getResultSize() {
            return resultSize;
        }

        public List<QuestionFormat> getFormats() {
            return formats;
        }
    }


}
