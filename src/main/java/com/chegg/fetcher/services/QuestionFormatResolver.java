package com.chegg.fetcher.services;

import com.chegg.fetcher.exceptions.FileFormatException;
import com.chegg.fetcher.dtos.QuestionFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Component
public class QuestionFormatResolver {

    private static final Logger LOGGER = LoggerFactory.getLogger(QuestionFormatResolver.class);

    public QuestionFormatResolver() {
    }

    public QuestionFormat resolve(String source) throws FileFormatException {
        Integer formatStartIndex = source.lastIndexOf(".");

        if (formatStartIndex.equals(-1)) {
            String err = String.format("Failed to find any format in URL [%s]", source);
            LOGGER.error(err);
            throw new FileFormatException(err);
        }
        String suffix = source.substring(formatStartIndex + 1);

        try {
            return QuestionFormat.valueOf(suffix.toUpperCase());
        } catch (IllegalArgumentException e) {
            String err = String.format("Failed to find supported format of type [%s]", suffix);
            LOGGER.error(err);
            throw new FileFormatException(err, e);
        }
    }
}
