package com.chegg.fetcher.utils;

import java.util.function.Function;

public class FunctionalUtils {

    public static  <T, R, E extends Exception> Function<T, R> exceptionWrapper(FunctionWithException<T, R, E> fe) {
        return arg -> {
            try {
                return fe.apply(arg);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };
    }
}
